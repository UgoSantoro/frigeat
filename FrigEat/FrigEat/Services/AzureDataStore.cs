﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using FrigEat.Models;
using System.IO;
using System.Threading;
using System.Net;
using System.Diagnostics;

namespace FrigEat.Services
{
    public class CResult<T>
    {
        public T data { get; set; }
        public bool success { get; set; }

        public int status { get; set; }
    }

    public class AzureDataStore : IDataStore<Item>
    {
        HttpClient client;

        public AzureDataStore()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri($"{App.AzureBackendUrl}/");

        }

        public async Task<T> Exec<T>(HttpMethod method, string uri)
        {
            try
            {

                var request = new HttpRequestMessage(method, uri);

                client.Timeout = TimeSpan.FromSeconds(20);
                var cancelTokenSource = new CancellationTokenSource();
                var cancelToken = cancelTokenSource.Token;


                var response = await client.SendAsync(request).ConfigureAwait(false);
                Console.WriteLine(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
                //if (response.StatusCode == HttpStatusCode.Unauthorized)
                  //  await ReAuthorize();
                if (!response.IsSuccessStatusCode)
                    throw new Exception("Unable to authenticate");


                CResult<T> cres = JsonConvert.DeserializeObject<CResult<T>>(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
                return cres.data;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return default(T);
            }
        }

        public async Task<bool> PostImageAsync(Stream source, string title, string description)
        {
            /*try
            {
                string req = _url + "image";


                var request = new HttpRequestMessage(HttpMethod.Post, req);
                request.Headers.Add("Authorization", GetAuthenticationHeader());
                request.Content = new StreamContent(source);

                Client.Timeout = TimeSpan.FromSeconds(30);
                var cancelTokenSource = new CancellationTokenSource();
                var cancelToken = cancelTokenSource.Token;


                var response = await Client.SendAsync(request).ConfigureAwait(false);
                Console.WriteLine(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                    await ReAuthorize();
                if (!response.IsSuccessStatusCode)
                    throw new Exception("Unable to authenticate");


                return response.IsSuccessStatusCode;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }*/
            return (false);
        }

        #region Gallery


        public List<Item> getItems(string section, string sort, string window, int page)
        {
            return (Exec<List<Item>>(HttpMethod.Get, "gallery/search/" + sort + "/" + window + "/" + page + "?q=" + section).Result);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            if (id != null)
            {
                var json = await client.GetStringAsync($"api/item/{id}");
                return await Task.Run(() => JsonConvert.DeserializeObject<Item>(json));
            }

            return null;
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            if (item == null)
                return false;

            var serializedItem = JsonConvert.SerializeObject(item);

            var response = await client.PostAsync($"api/item", new StringContent(serializedItem, Encoding.UTF8, "application/json"));

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            if (item == null || item.Id == null)
                return false;

            var serializedItem = JsonConvert.SerializeObject(item);
            var buffer = Encoding.UTF8.GetBytes(serializedItem);
            var byteContent = new ByteArrayContent(buffer);

            var response = await client.PutAsync(new Uri($"api/item/{item.Id}"), byteContent);

            return response.IsSuccessStatusCode;
        }
        #endregion

        public async Task<bool> DeleteItemAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                return false;

            var response = await client.DeleteAsync($"api/item/{id}");

            return response.IsSuccessStatusCode;
        }

        Task<bool> IDataStore<Item>.AddItemAsync(Item item)
        {
            throw new NotImplementedException();
        }

        Task<bool> IDataStore<Item>.UpdateItemAsync(Item item)
        {
            throw new NotImplementedException();
        }

        Task<bool> IDataStore<Item>.DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        Task<Item> IDataStore<Item>.GetItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<Item>> IDataStore<Item>.GetItemsAsync(bool forceRefresh)
        {
            throw new NotImplementedException();
        }
    }
}