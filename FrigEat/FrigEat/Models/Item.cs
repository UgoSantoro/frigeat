﻿using System;
using System.Collections.Generic;

namespace FrigEat.Models
{
    public enum Category
    {
        Boissons,
        CorpsGras,
        Féculents,
        Conserves,
    }
    public class Item
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime ExpirationDate { get; set; }
        public List<Tag> Tags;
    }
}